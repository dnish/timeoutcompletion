﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using log4net;

namespace GPICompletionManagement
{
    internal class TransactionManagement
    {
        internal async void CheckForTransactionsEventHandler(object source, ElapsedEventArgs e)
        {
            //string strSqlCommand = "SELECT TerminalId, TransDate, TransNumber, CardType, DataEncryptionKey, AESCompletionRequest"
            //                        + " FROM SAF_Transactions WHERE CardType <> 23 ORDER BY TransDate";

			string strSqlCommand = "SELECT TOP(1) SAF_Transactions.TerminalId, SAF_Transactions.TransDate, SAF_Transactions.TransNumber, SAF_Transactions.CardType, " + 
								   "SAF_Transactions.DataEncryptionKey, SAF_Transactions.AESCompletionRequest, card_readers.pumpNumber " +
								   "FROM SAF_Transactions " +
								   "INNER JOIN Card_Readers " +
								   "ON SAF_Transactions.TerminalId = card_readers.terminalID " +
								   "WHERE SAF_Transactions.CardType <> 23 AND SAF_Transactions.AESCompletionRequest IS NOT NULL ORDER BY SAF_Transactions.TransNumber";


			using (SqlConnection conn = new SqlConnection(Properties.databaseConnection))
            {
                using (SqlCommand cmd = new SqlCommand(strSqlCommand, conn))
                {
                    conn.Open();

                    using (SqlDataAdapter adp = new SqlDataAdapter(cmd))
                    {
                        DataTable dt = new DataTable();
                        adp.Fill(dt);

                        int rowCount = dt.Rows.Count;

						if (rowCount > 0)
						{
							Transaction trans = new Transaction();

							trans = new Transaction();

							if (!dt.Rows[0].IsNull("TerminalId"))
							{
								trans.terminalID = dt.Rows[0].Field<string>("TerminalId");
							}
							if (!dt.Rows[0].IsNull("TransDate"))
							{
								trans.transDate = dt.Rows[0].Field<DateTime>("TransDate");
							}
							if (!dt.Rows[0].IsNull("TransNumber"))
							{
								trans.transNumber = dt.Rows[0].Field<string>("TransNumber");
							}
							if (!dt.Rows[0].IsNull("CardType"))
							{
								trans.cardType = dt.Rows[0].Field<Int16>("CardType");
							}
							if (!dt.Rows[0].IsNull("DataEncryptionKey"))
							{
								trans.dataEncryptionKey = dt.Rows[0].Field<string>("DataEncryptionKey");
							}
							if (!dt.Rows[0].IsNull("AESCompletionRequest"))
							{
								using (SqlConnection conn2 = new SqlConnection(Properties.databaseConnection))
								{
									string sqlCmd = String.Format("SELECT one, two FROM Timeout_Completion WHERE transNum = {0}", trans.transNumber);
									using (SqlCommand cmd2 = new SqlCommand(sqlCmd, conn2))
									{
										conn2.Open();

										using (SqlDataAdapter adp2 = new SqlDataAdapter(cmd2))
										{
											DataTable dt2 = new DataTable();
											adp2.Fill(dt2);

											trans.completionRequest = DecryptStringFromBytes_Aes(dt.Rows[0].Field<byte[]>("AESCompletionRequest"), dt2.Rows[0].Field<byte[]>("one"), dt2.Rows[0].Field<byte[]>("two"));
										}
									}
								}
								//trans[c].completionRequest = dt.Rows[c].Field<string>("CompletionRequest");
							}
							if (!dt.Rows[0].IsNull("pumpNumber"))
							{
								trans.readerPos = dt.Rows[0].Field<Int16>("pumpNumber");
							}

                            if (trans.transDate.AddHours(1) < DateTime.Now)
                            {

                            }
							TenderRetail tr = new TenderRetail();
							await Task.Run(() => tr.connectToMCM(trans.readerPos, trans.transNumber, trans.completionRequest));


							//Transaction[] trans = new Transaction[rowCount];

							//for (int c = 0; c < rowCount; c++)
							//{
							//	trans[c] = new Transaction();

							//	if (!dt.Rows[c].IsNull("TerminalId"))
							//	{
							//		trans[c].terminalID = dt.Rows[c].Field<string>("TerminalId");
							//	}
							//	if (!dt.Rows[c].IsNull("TransDate"))
							//	{
							//		trans[c].transDate = dt.Rows[c].Field<DateTime>("TransDate");
							//	}
							//	if (!dt.Rows[c].IsNull("TransNumber"))
							//	{
							//		trans[c].transNumber = dt.Rows[c].Field<string>("TransNumber");
							//	}
							//	if (!dt.Rows[c].IsNull("CardType"))
							//	{
							//		trans[c].cardType = dt.Rows[c].Field<Int16>("CardType");
							//	}
							//	if (!dt.Rows[c].IsNull("DataEncryptionKey"))
							//	{
							//		trans[c].dataEncryptionKey = dt.Rows[c].Field<string>("DataEncryptionKey");
							//	}
							//	if (!dt.Rows[c].IsNull("AESCompletionRequest"))
							//	{
							//		using (SqlConnection conn2 = new SqlConnection(Properties.databaseConnection))
							//		{
							//			string sqlCmd = "SELECT one, two FROM Timeout_Completion WHERE transNum = " + trans[c].transNumber;
							//			using (SqlCommand cmd2 = new SqlCommand(sqlCmd, conn2))
							//			{
							//				conn2.Open();

							//				using (SqlDataAdapter adp2 = new SqlDataAdapter(cmd2))
							//				{
							//					DataTable dt2 = new DataTable();
							//					adp2.Fill(dt2);

							//					trans[c].completionRequest = DecryptStringFromBytes_Aes(dt.Rows[c].Field<byte[]>("AESCompletionRequest"), dt2.Rows[c].Field<byte[]>("one"), dt2.Rows[c].Field<byte[]>("two"));
							//				}
							//			}
							//		}
							//		//trans[c].completionRequest = dt.Rows[c].Field<string>("CompletionRequest");
							//	}
							//	if (!dt.Rows[c].IsNull("pumpNumber"))
							//	{
							//		trans[c].readerPos = dt.Rows[c].Field<Int16>("pumpNumber");
							//	}

							//	TenderRetail tr = new TenderRetail();
							//	await Task.Run(() => tr.connectToMCM(trans[c].readerPos, trans[c].transNumber, trans[c].completionRequest));
							//}
						}
						else
						{
							startTransactionTimer();
						}

                    }
                }
            }
        }

        static string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;

        }

        static byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key,byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Key;
                aesAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))                        
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream.
            return encrypted;

        }

		internal void startTransactionTimer()
		{
			Properties transProp = new Properties();

			try
			{
				//Configure and start timer to check for time-out completions
				int interval = 120000;
				transProp.checkForTransactions = new System.Timers.Timer(interval);
				transProp.checkForTransactions.Elapsed += new  System.Timers.ElapsedEventHandler(CheckForTransactionsEventHandler);
				transProp.checkForTransactions.AutoReset = false;
				transProp.checkForTransactions.Enabled = true;
				transProp.checkForTransactions.Start();
			}
			catch (Exception e)
			{
				SocketAsync.SocketClient.TransMng.Error(string.Format("Error: {0}", e.Message));
			}
		}
    }
}
