﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using GPICompletionManagement;

namespace SocketAsync
{
	public class SocketClient
	{
        internal static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

		internal static readonly log4net.ILog TransMng = log4net.LogManager.GetLogger("TransMngment");
		TcpConnection mClient = new TcpConnection();
		internal static Dictionary<int, FinalizeTrxn> dictionaryFinalizeTrxn = new Dictionary<int, FinalizeTrxn>();

		public SocketClient()
		{
		}

		internal static IPAddress ServerIPAddress { get; set; }

        internal static int ServerPort { get; set; }
		
		public bool SetServerIPAddress(string _IPAddressServer)
		{
			IPAddress ipaddr = null;

			if (!IPAddress.TryParse(_IPAddressServer, out ipaddr))
			{
                TransMng.Debug("Invalid server IP supplied.");
				Console.WriteLine("Invalid server IP supplied.");
				return false;
			}

            ServerIPAddress = ipaddr;

			return true;
		}

		public bool SetPortNumber(string _ServerPort)
		{
			int portNumber = 0;

			if (!int.TryParse(_ServerPort.Trim(), out portNumber))
			{
                TransMng.Debug("Invalid port number supplied, return.");
				Console.WriteLine("Invalid port number supplied, return.");
				return false;
			}

			if (portNumber <= 0 || portNumber > 65535)
			{
                TransMng.Debug("Port number must be between 0 and 65535.");
				Console.WriteLine("Port number must be between 0 and 65535.");
				return false;
			}

            ServerPort = portNumber;

			return true;
		}

		internal async Task sendPP08Response(string data, XDocument doc, IEnumerable<XElement> verifyMAC)
		{
			try
			{
				XNamespace ns = XNamespace.Get("http://tempuri.org/TerminalCommands.xsd");

				var result = verifyMAC.Attributes("Valid").Single().Value;

				var readerPos = doc.Root.Element(ns + "TerminalResponse").Attribute("TerminalId").Value;

				GPICompletionManagement.TcpConnection socket = new GPICompletionManagement.TcpConnection();

				if (result == "true")
				{
					string request = string.Format("{0}PP300801{1}L0{2}", (char)0x02, (char)0x1E, (char)0x01E);
					await SendToServer(Convert.ToInt16(readerPos), request);
					SocketAsync.SocketClient.TransMng.Debug("PP08 Send to MCM");
				}
				else
				{
				}
			}
			catch (Exception e)
			{

				SocketClient.TransMng.Error(string.Format("Error: {0}", e.Message));
			}
		}

		internal async Task sendPP07Response(string data, XDocument doc, IEnumerable<XElement> t)
		{
			try
			{
				XNamespace ns = XNamespace.Get("http://tempuri.org/TerminalCommands.xsd");

				var MAC = t.Attributes("MACValue").Single().Value;

				var readerPos = doc.Root.Element(ns + "TerminalResponse").Attribute("TerminalId").Value;

				GPICompletionManagement.TcpConnection socket = new GPICompletionManagement.TcpConnection();


				string request = string.Format("{0}PP300701{1}G{2}{3}", (char)0x02, (char)0x1E, MAC, (char)0x01E);
				await SendToServer(Convert.ToInt16(readerPos), request);
				SocketAsync.SocketClient.TransMng.Debug("PP07 Send to MCM");
			}
			catch (Exception e)
			{

				SocketClient.TransMng.Error(string.Format("Error: {0}", e.Message));
			}
		}

		public void CloseAndDisconnect(int readerPos)
		{
			if (mClient[readerPos, 0] != null)
			{
				if (mClient[readerPos, 0].Connected)
				{
					mClient[readerPos, 0].Client.Shutdown(SocketShutdown.Send);
					mClient[readerPos, 0].Close();
				}

				mClient[readerPos, 1] = null;
			}
		}

		public async Task SendToServer(int readerPos, string strInputUser, string transNum)
		{
			if (string.IsNullOrEmpty(strInputUser))
			{
				TransMng.Debug("Empty string supplied to send.");
				return;
			}
			if (mClient[readerPos, 0] != null)
			{
				if (mClient[readerPos, 0].Connected)
				{
					StreamWriter clientStreamWriter = new StreamWriter(mClient[readerPos, 0].GetStream());
					clientStreamWriter.AutoFlush = true;

					await clientStreamWriter.WriteAsync(strInputUser);

					TransMng.Info(string.Format("Data sent: Completion sent for {0}", transNum));

				}
			}

		}

		public async Task SendToServer(int readerPos, string strInputUser)
		{
			if (string.IsNullOrEmpty(strInputUser))
			{
				TransMng.Debug("Empty string supplied to send.");
				return;
			}
			if (mClient[readerPos, 0] != null)
			{
				if (mClient[readerPos, 0].Connected)
				{
					StreamWriter clientStreamWriter = new StreamWriter(mClient[readerPos, 0].GetStream());
					clientStreamWriter.AutoFlush = true;

					await clientStreamWriter.WriteAsync(strInputUser);

					TransMng.Info(string.Format("Data sent: {0}", strInputUser));
				}
			}

		}

		public async Task ConnectToServer(int readerPos, string transNum)
		{
			if (mClient[readerPos, 0] == null)
			{
				mClient[readerPos, 0] = new TcpClient();
			}

			try
			{
				await mClient[readerPos, 0].ConnectAsync(ServerIPAddress, ServerPort);
				TransMng.Info(string.Format("Connected to server IP/Port: {0}, {1}", ServerIPAddress, ServerPort));
				StateObject state = new StateObject();
				state.readerPos = readerPos;
				state.BufferSize = 2048;
				state.buffer = new byte[state.BufferSize];
				state.transNum = transNum;

				ReadDataAsync(state);
			}
			catch (Exception e)
			{
				TransMng.Error(e.ToString());
				Console.WriteLine(e.ToString());
				throw;
			}
		}

		private async void ReadDataAsync(StateObject state)
		{
			TransactionManagement transactionManagement = new TransactionManagement();

			try
			{
				state.workSocket = mClient[state.readerPos, 0].GetStream();
				int readByteCount = 0;
                int trial = 0;

				while (true)
				{
					readByteCount = await state.workSocket.ReadAsync(state.buffer, 0, state.buffer.Length);
                    string message = Encoding.UTF8.GetString(state.buffer, 0, readByteCount);
                    if (message.IndexOf("TR2") != -1)
                    {
                        int startIndex = message.IndexOf("TR2");
                        int endIndex = message.IndexOf("INV:");
                        int countIndex = endIndex - startIndex;
                        message = message.Remove(startIndex, countIndex);
                    }

                    TransMng.Info(string.Format("Received bytes: {0} - Message: {1}", readByteCount, message));

					state.sb.Append(Encoding.UTF8.GetString(state.buffer, 0, readByteCount));

					IEnumerable<string> record = new List<string>();
					char[] delimiters = { (char)0x02, (char)0x06, (char)0x15 };
					record = state.sb.ToString().Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

					if (record.Count() > 0 && record.ElementAt(0) != null && record.ElementAt(0) != String.Empty)
					{
						int t = record.ElementAt(0).Length;
						int s = state.sb.Length;

						//Remove record from string
						TransMng.Debug(string.Format("Record Length = {0}; Stream Length = {1}", t, s));
						if (t == 1)
						{
							TransMng.Debug(string.Format("Record Length is 1. Message = {0}", record.ElementAt(0)));
							state.sb.Remove(0, record.ElementAt(0).Length);
						}
						else
						{
                            string removeMessage = String.Empty;

                            if (record.ElementAt(0).IndexOf("TR2") != -1)
                            {
                                int startIndex = record.ElementAt(0).IndexOf("TR2");
                                int endIndex = record.ElementAt(0).IndexOf("INV:");
                                int count = endIndex - startIndex;
                                removeMessage = record.ElementAt(0).Remove(startIndex, count);
                            }
                            else
                            {
                                removeMessage = record.ElementAt(0);
                            }
                            TransMng.Debug(string.Format("Remove from stream: {0}", removeMessage));
                            state.sb.Remove(0, record.ElementAt(0).Length + 1);
						}

						//Parse the response from MCM
						GPICompletionManagement.TenderRetail MCM = new GPICompletionManagement.TenderRetail();
						var result = await MCM.parseMCMResponse(record.ElementAt(0).ToString(), state);

						if (await processFinalizeTrxn(dictionaryFinalizeTrxn.ElementAt(result).Value, state, transactionManagement))
						{
							break;
						}
					}
                    else
                    {
                        trial++;
                        TransMng.Debug(String.Format("Record Count = 0. Trial = {0}", trial));
                        if (trial > 5)
                        {
                            CloseAndDisconnect(state.readerPos);
                            state = null;
                            TransMng.Debug(string.Format("Restarting transaction timer.{0}", Environment.NewLine));
                            transactionManagement.startTransactionTimer();
                            break;
                        }
                    }
				}
			}
			catch (Exception e)
            {
                TransMng.Error(e.ToString());
				transactionManagement.startTransactionTimer();
            }
		}

		internal async Task<bool> processFinalizeTrxn(FinalizeTrxn result, StateObject state, TransactionManagement transactionManagement)
		{
			var transactionFinalized = await result.finalizeTrxn(state, transactionManagement);
			return transactionFinalized;
		}

		internal void MoveTrxnToAlertTable(string transNum)
		{
			try
			{
				List<string> sqlCommands = new List<string>();
				sqlCommands.Add(string.Format("INSERT INTO SAF_Transactions_Alert SELECT * from SAF_Transactions WHERE transNumber = '{0}'", transNum));
				sqlCommands.Add(string.Format("DELETE SAF_Transactions WHERE transNumber = '{0}'", transNum));
				//string strSqlCommand = string.Format("INSERT INTO SAF_Transactions_Alert SELECT * from SAF_Transactions WHERE transNumber = '{0}'", transNum);

				using (SqlConnection conn = new SqlConnection(Properties.databaseConnection))
				{
					conn.Open();
					SqlTransaction sqlTransaction;
					sqlTransaction = conn.BeginTransaction();

					foreach(string c in sqlCommands)
						{
						using (SqlCommand cmd = new SqlCommand(c, conn, sqlTransaction))
						{
							cmd.ExecuteNonQuery();
						}
					}

					sqlTransaction.Commit();
				}

			}
			catch (Exception)
			{

				throw;
			}
		}

		internal void RemoveTrxnFromTable(string transNum)
		{
			try
			{
				string strSqlCommand = string.Format("DELETE SAF_Transactions WHERE transNumber = '{0}'", transNum);

				using (SqlConnection conn = new SqlConnection(Properties.databaseConnection))
				{
					using (SqlCommand cmd = new SqlCommand(strSqlCommand, conn))
					{
						conn.Open();
						cmd.ExecuteNonQuery();
					}
				}
			}
			catch (Exception e)
			{
                TransMng.Error(string.Format("Remove Transaction From Database Error: {0}", e.Message));
                throw;
			}
		}
	}

}
