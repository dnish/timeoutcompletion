﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using SocketAsync;

namespace GPICompletionManagement
{
    public partial class FMSTransMngService : ServiceBase
    {
		//internal System.Timers.Timer checkForTransactions = new System.Timers.Timer();
		//Properties props = new Properties();

		public FMSTransMngService()
        {
            InitializeComponent();
		}

        protected override async void OnStart(string[] args)
        {
			//System.Diagnostics.Debugger.Launch();

			TransactionManagement transMngInstance = new TransactionManagement();

			transMngInstance.startTransactionTimer();

            await Task.Run(() => getSQLServer());
			await Task.Run(() => getMCMConnectionData());
        }

        protected override void OnStop()
        {
			Properties props = new Properties();
			props.checkForTransactions.Stop();
			props.checkForTransactions.Dispose();
        }

		private void getMCMConnectionData()
		{
			//Get the sql database connection string from the activedb.txt file
			try
			{
				Encoding codepage = System.Text.Encoding.GetEncoding(1252);
				var multiSrvRead = "C:\\sro\\multiSrv.txt";
				string ipaddr = String.Empty;
				string port = String.Empty;

				using (StreamReader sr = new StreamReader(multiSrvRead, codepage))
				{
					int count = 0;

					while (sr.Peek() >= 0)
					{
						var readLine = sr.ReadLine();
                        Properties.TransMng.Debug(readLine);
						Console.WriteLine(readLine);
						if (readLine.StartsWith("IP"))
						{
							ipaddr = readLine.Substring(10);
						}

						if (readLine.StartsWith("PORT:"))
						{
							port = readLine.Substring(5);
						}

						count++;
					}
				}

				SocketClient socketClient = new SocketClient();
				socketClient.SetServerIPAddress(ipaddr);
				socketClient.SetPortNumber(port);

				}
			catch (Exception e)
			{
				SocketClient.TransMng.Error(string.Format("Error: {0}", e.Message));
			}
		}


		private void getSQLServer()
		{
			//Get the sql database connection string from the activedb.txt file
			try
			{
				Encoding codepage = System.Text.Encoding.GetEncoding(1252);
				var activeDBRead = "C:\\sro\\activedb.txt";

				String databaseInstance = String.Empty;
				string userID = String.Empty;
				string password = String.Empty;

				using (StreamReader sr = new StreamReader(activeDBRead, codepage))
				{
					int count = 0;

					while (count < 4)
					{
						if (count == 1)
						{
							userID = sr.ReadLine();
						}
						else if (count == 2)
						{
							password = DecryptPassword(sr.ReadLine());
						}
						else if (count == 3)
						{
							databaseInstance = sr.ReadLine();
						}
						else
						{
							sr.ReadLine();
						}
						count++;
					}

					sr.Close();
				}

				Properties.databaseConnection = @"Data Source=" + databaseInstance + ";Initial Catalog=sro;User Id=" + userID + ";Password=" + password + ";MultipleActiveResultSets=True";
			}
			catch (Exception e)
			{
				SocketClient.TransMng.Error(string.Format("Error: {0}", e.Message));
			}
		}

        protected string DecryptPassword(string sPassword)
        {
            Encoding codepage = System.Text.Encoding.GetEncoding(1252);
            byte[] asciiBytes = Encoding.GetEncoding(1252).GetBytes(sPassword);
            string strResult = "";
            int j = 100;
            int intCode;
            for (int i = 1; i <= sPassword.Length; i++)
            {
                intCode = asciiBytes[i - 1] - j++;
                strResult += (char)intCode;
            }

            return strResult;
        }
    }
}
