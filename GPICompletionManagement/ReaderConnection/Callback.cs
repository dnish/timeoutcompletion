﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using ConnectReader;
using System.Xml.Linq;

namespace TestWayneReader
{
    [System.ServiceModel.CallbackBehavior(ConcurrencyMode = System.ServiceModel.ConcurrencyMode.Reentrant, UseSynchronizationContext = false)]
    class Callback : IReaderConnectCallback
	{
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
    (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void sendTerminalEvent(int readerPOS, string data)
        {
			
			SocketAsync.SocketClient.TransMng.Debug(string.Format("Data Received from Reader: {0}", data));
        }

        public void sendTerminalCommand(int readerPOS, string data)
        {
			SocketAsync.SocketClient.TransMng.Info(string.Format("Data Received from Reader: {0}", data));
        }

		public void sendTerminalResponse(int readerPOS, string data)
		{
			SocketAsync.SocketClient socketClient = new SocketAsync.SocketClient();

			XDocument doc = XDocument.Parse(data);
			XNamespace ns = XNamespace.Get("http://tempuri.org/TerminalCommands.xsd");

			var calculateMACResponse = doc.Descendants().Where(n => n.Name == (ns + "CalculateMACResponse"));
			if (calculateMACResponse.Count() > 0)
			{
				socketClient.sendPP07Response(data, doc, calculateMACResponse);
			}

			var verifyMAC = doc.Descendants().Where(n => n.Name == (ns + "VerifyMACResponse"));
			if (verifyMAC.Count() > 0)
			{
				socketClient.sendPP08Response(data, doc, verifyMAC);
			};
		}

        public void sendCATStatus(int readerPOS, int data)
        {
            log.Warn("Reader " + readerPOS + " status is now " + data);
            //errorCheck(DateTime.Now.ToString() + ": Data Received from Reader " + readerPOS + ": " + data + Environment.NewLine, "ReaderStatus_");
        }

        public void sendExactResponse(int readerPOS, int exactPOS, byte[] response, byte commandCode)
        {
            
        }

        public void sendExactStatus(int exactPos, int readerPos, int exactConnectedStatus)
        {
            log.Info("Exact status = " + exactConnectedStatus);
        }
    }
}
