﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using ConnectReader;
using TestWayneReader;
using SocketAsync;

namespace GPICompletionManagement.ReaderConnection
{
	class WayneConnect
	{
		InstanceContext instance = new InstanceContext(new Callback());
		private Task<string> Connected { get; set; }
		private DuplexChannelFactory<iReaderConnect> _netTcpProxy;
		public static iReaderConnect client;

		public ConnectReader.WayneCAT proxy;

		Callback callback = new Callback();

		public static string rcvID;

		public DuplexChannelFactory<iReaderConnect> NetTcpProxy
		{
			get
			{
				return _netTcpProxy;
			}
			set
			{
				_netTcpProxy = value;
			}
		}

		public void connectNetTCP()
		{
			try
			{
				proxy = new WayneCAT();
				NetTcpBinding binding = new NetTcpBinding()
				{
					Security = new NetTcpSecurity()
					{
						Mode = SecurityMode.Transport,
						Transport = new TcpTransportSecurity
						{
							ClientCredentialType = TcpClientCredentialType.Windows
						}
					},
					ReceiveTimeout = TimeSpan.FromMinutes(10),
					SendTimeout = TimeSpan.FromMinutes(10),
					CloseTimeout = TimeSpan.FromMinutes(10),
					OpenTimeout = TimeSpan.FromMinutes(10)
				};

				binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.Windows;
				binding.Security.Transport.ProtectionLevel = System.Net.Security.ProtectionLevel.EncryptAndSign;

				//NetTcpBinding
				//EndpointAddress myEndpointAddress = new EndpointAddress(new Uri(@"net.tcp://" + nodeAddressTextBox.Text + ":8525/connectWayneWCF/WayneCAT/"), EndpointIdentity.CreateDnsIdentity("localhost"));
				EndpointAddress myEndpointAddress = new EndpointAddress(new Uri(@"net.tcp://" + System.Windows.Forms.SystemInformation.ComputerName + ":8525/connectWayneWCF/WayneCAT/"), EndpointIdentity.CreateDnsIdentity("localhost"));

				NetTcpProxy = new DuplexChannelFactory<iReaderConnect>(callback, binding, myEndpointAddress);

				try
				{
					client = NetTcpProxy.CreateChannel(instance);
					rcvID = "TransManagement";
					client.RegisterClient(rcvID);
				}
				catch (Exception er)
				{
					SocketClient.TransMng.Error(string.Format("Channel Factory Error: {0}", er.Message));
				}

			}
			catch (Exception e)
			{
				SocketClient.TransMng.Error(string.Format("Error: {0}", e.Message));
			}
		}
	}
}
