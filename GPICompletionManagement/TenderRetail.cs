﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using SocketAsync;

namespace GPICompletionManagement
{
    public class PPRequest
    {
        public virtual void ppRequest(Properties props, StateObject state)
        {
			try
			{
				ReaderConnection.WayneConnect connect = new ReaderConnection.WayneConnect();
				connect.connectNetTCP();

				SocketClient.TransMng.Debug(string.Format("PP Value H in ASCII: {0}", props.dictionaryPPMessages["H"]));
				string MACString = TenderRetail.AsciiToHex(props.dictionaryPPMessages["H"]);
				SocketClient.TransMng.Debug(string.Format("PP Value H in HEX: {0}", MACString));

				Properties.sequenceNumber = Properties.sequenceNumber + 1;
				string strCommand = string.Format("<SecurityModuleCommand><CalculateMACCommand SequenceNumber = '{0}'>{1}</CalculateMACCommand></SecurityModuleCommand>", Properties.sequenceNumber, MACString);
				SocketClient.TransMng.Debug(string.Format("MAC Command: {0}", strCommand));
				ReaderConnection.WayneConnect.client.sendCommand(state.readerPos, strCommand, "TransManagement", Properties.sequenceNumber);
			}
			catch (Exception e)
			{
				SocketClient.TransMng.Error(string.Format("Error: {0}", e.Message));
			}
		}
    }

	public class PP08 : PPRequest
    {
        public override void ppRequest(Properties props, StateObject state)
        {
			try
			{
				string MACValue = props.dictionaryPPMessages["G"];
				SocketClient.TransMng.Debug(string.Format("PP Value G in HEX: {0}", MACValue));

				SocketClient.TransMng.Debug(string.Format("PP Value H in ASCII: {0}", props.dictionaryPPMessages["H"]));
				string MACString = TenderRetail.AsciiToHex(props.dictionaryPPMessages["H"]);
				SocketClient.TransMng.Debug(string.Format("PP Value H in HEX: {0}", MACString));


				Properties.sequenceNumber = Properties.sequenceNumber + 1;
				string strCommand = string.Format("<SecurityModuleCommand><VerifyMACCommand SequenceNumber = '{0}' MACValue = '{1}'>{2}</VerifyMACCommand></SecurityModuleCommand>", Properties.sequenceNumber, MACValue, MACString);
				SocketClient.TransMng.Debug(string.Format("MAC Command: {0}", strCommand));
				ReaderConnection.WayneConnect.client.sendCommand(state.readerPos, strCommand, "TransManagement", Properties.sequenceNumber);
			}
			catch (Exception e)
			{
				SocketClient.TransMng.Error(string.Format("Error: {0}", e.Message));
			}
        }
    }

    public class PP09 : PPRequest
    {
        public override void ppRequest(Properties props, StateObject state)
        {

        }
    }

    class TenderRetail
    {
        internal async Task<int> parseMCMResponse(string completion, StateObject state)
        {
			//IEnumerable<string> completionResult = new List<string> { "1[49]", "2[50]" };
			IEnumerable<string> completionError = new List<string>
			{
				"0[48]",
				"<[60]",
				"A[65]",
				"C[67]",
				"E[69]",
				"F[70]",
				"G[71]",
				"H[72]",
				"I[73]",
				"J[74]",
				"K[75]",
				"L[76]",
				"M[77]",
				"N[78]",
				"O[79]",
				"P[80]",
				"Q[81]",
				"R[82]",
				"S[83]",
				"T[84]",
				"U[85]",
				"V[86]",
				"X[88]",
				"Y[89]",
				"a[97]",
				"b[98]",
				"d[100]",
				"e[101]",
				"g[103]",
				"h[104]",
				"i[105]",
				"j[106]",
				"k[107]",
				"l[108]",
				"m[109]",
				"n[110]",
				"o[111]",
				"p[112]",
				"q[113]",
				"r[114]",
				"s[115]",
				"t[116]",
				"u[117]",
				"w[119]",
				"y[121]",
				"z[122]"
			};

			if (completion.Contains("1[49]"))
			{
				return 0;
			}
			else if (completion.Contains("2[50]"))
			{
				return 1;
			}
			else if (completionError.Any(x => completion.Contains(x)))
			{
				return 3;
			}

            for (int i = 0; i < 3; i++)
            {
                if (completion.Contains(Properties.dictionaryPPRequest.ElementAt(i).Key))
                {
                    Properties props = new Properties();
					char[] delim = { (char)0x1E };
					props.ppRequestElements = completion.Split(delim, StringSplitOptions.RemoveEmptyEntries);
					props.dictionaryPPMessages.Clear();

					foreach (string a in props.ppRequestElements)
					{
						if (Properties.dictionaryPPRequest.Keys.Any(x => a.Contains(x)))
						{
							props.dictionaryPPMessages.Add(a.Substring(3, 6), a);
						}
						else
						{
							if (a != "\0")
							{
								string MACString = a.Substring(1, a.Length - 1);
								int intRemainder = MACString.Length % 8;
								if (intRemainder > 0)
								{
									MACString = MACString.PadRight(MACString.Length + (8 - intRemainder), '\0');
								}

								props.dictionaryPPMessages.Add(a.Substring(0, 1), MACString);
							}
						}
					}

					processPPRequest(Properties.dictionaryPPRequest.ElementAt(i).Value, props, state);
					return 2;
                }
            }

            return 2;
        }

        internal async Task connectToMCM(int readerPos, string transNum, string completionRequest)
        {
            SocketClient client = new SocketClient();
            await client.ConnectToServer(readerPos, transNum);
            await client.SendToServer(readerPos, completionRequest, transNum);
            
        }
        
        public void processPPRequest(PPRequest request, Properties props, StateObject state)
        {
            request.ppRequest(props, state);
        }

		public static string AsciiToHex(string sSource)
		{
			StringBuilder sb = new StringBuilder();
			foreach (char c in sSource)
				sb.AppendFormat("{0:X2}", (int)c);

			return sb.ToString().Trim();
		}
	}
}
