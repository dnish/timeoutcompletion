﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace GPICompletionManagement
{
    public class Properties
    {
        internal static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        internal static readonly log4net.ILog TransMng = log4net.LogManager.GetLogger("TransMngment");
        //public static string stLogFileDirectory = @"c:\sro\RMS_LOGS\";

        internal static string databaseConnection { get; set; }

        internal static Dictionary<string, PPRequest> dictionaryPPRequest = new Dictionary<string, PPRequest>();
		internal Dictionary<string, string> dictionaryPPMessages = new Dictionary<string, string>();

        internal IEnumerable<string> ppRequestElements = new List<string>();

		internal static int _sequenceNumber;
		internal static int sequenceNumber
		{
			get
			{
				return _sequenceNumber;
			}
			set
			{
				if (_sequenceNumber > 200 || _sequenceNumber < 32768)
				{
					_sequenceNumber = value;
				}
				else
				{
					_sequenceNumber = 200;
				}
			}
		}

		internal System.Timers.Timer checkForTransactions = new System.Timers.Timer();

		public Properties()
        {
            dictionaryPPRequest.Clear();
            dictionaryPPRequest.Add("PP3007", new PPRequest());
            dictionaryPPRequest.Add("PP3008", new PP08());
            dictionaryPPRequest.Add("PP3009", new PP09());

			SocketAsync.SocketClient.dictionaryFinalizeTrxn.Clear();
			SocketAsync.SocketClient.dictionaryFinalizeTrxn.Add(0, new FinalizeTrxn());
			SocketAsync.SocketClient.dictionaryFinalizeTrxn.Add(1, new FinalizeTrxnOffline());
			SocketAsync.SocketClient.dictionaryFinalizeTrxn.Add(2, new SendPPResponse());
			SocketAsync.SocketClient.dictionaryFinalizeTrxn.Add(3, new FinalizeTrxnError());
		}

	}

    public class Transaction
    {
        internal string terminalID { get; set; }
        internal DateTime transDate { get; set; }
        internal string transNumber { get; set; }
        internal int cardType { get; set; }
        internal string dataEncryptionKey { get; set; }
        internal string completionRequest { get; set; }
		internal int readerPos { get; set; }

    }

	public class StateObject
	{
		//public NetworkStream workSocket = null;
		public NetworkStream workSocket { get; set; }
		//public int BufferSize = 2;
		public int BufferSize { get; set; }
		public byte[] buffer { get; set; }
		public StringBuilder sb = new StringBuilder();
		public int readerPos { get; set; }
		public string transNum { get; set; }
		public string terminalID { get; set; }
		public string response { get; set; }
	}

	public class TcpConnection
	{
		public static Dictionary<int, TcpClient> _tcpClient = new Dictionary<int, TcpClient>();

		public TcpClient this[int readerPos, int close]
		{
			get
			{
				if (_tcpClient.ContainsKey(readerPos))
				{
					return _tcpClient[readerPos];
				}
				else
				{
					return null;
				}
			}
			set
			{
				if (close == 0 && !_tcpClient.ContainsKey(readerPos))
				{
					_tcpClient.Add(readerPos, value);
					//_tcpClient[readerPos] = value;
				}
				else
				{
					_tcpClient.Remove(readerPos);
				}
			}
		}
	}

	public class FinalizeTrxn : SocketAsync.SocketClient
	{
		internal async virtual Task<bool> finalizeTrxn(StateObject state, TransactionManagement transactionManagement)
		{
			try
			{
				//Send ACK to MCM
				char ack = (char)0x06;
				await SendToServer(state.readerPos, ack.ToString());
				TransMng.Info(string.Format("Transaction {0} Approved", state.transNum));
				TransMng.Debug("Disconnected from server.");
				CloseAndDisconnect(state.readerPos);

				RemoveTrxnFromTable(state.transNum);

				state = null;
				TransMng.Debug(string.Format("Restarting transaction timer.{0}", Environment.NewLine));
				transactionManagement.startTransactionTimer();
				return true;
			}
			catch (Exception e)
			{
				TransMng.Error(string.Format("Error: {0}", e.Message));
				return false;
			}
		}
	}

	public class FinalizeTrxnOffline : FinalizeTrxn
	{
		internal async override Task<bool> finalizeTrxn(StateObject state, TransactionManagement transactionManagement)
		{
			try
			{
				//Send ACK to MCM
				char ack = (char)0x06;
				await SendToServer(state.readerPos, ack.ToString());
				TransMng.Info(string.Format("Transaction {0} Approved Offline", state.transNum));
				TransMng.Debug("Disconnected from server.");
				CloseAndDisconnect(state.readerPos);

				state = null;
				TransMng.Debug(string.Format("Restarting transaction timer.{0}", Environment.NewLine));
				transactionManagement.startTransactionTimer();
				return true;
			}
			catch (Exception e)
			{
				TransMng.Error(string.Format("Error: {0}", e.Message));
				return false;
			}

		}
	}

	public class FinalizeTrxnError : FinalizeTrxn
	{
		internal async override Task<bool> finalizeTrxn(StateObject state, TransactionManagement transactionManagement)
		{
			try
			{
				TransMng.Info(string.Format("Transaction {0} Declined", state.transNum));
				CloseAndDisconnect(state.readerPos);
				MoveTrxnToAlertTable(state.transNum);
				state = null;
				TransMng.Debug(string.Format("Restarting transaction timer.{0}", Environment.NewLine));
				transactionManagement.startTransactionTimer();
				return true;
			}
			catch (Exception e)
			{
				TransMng.Error(string.Format("Error: {0}", e.Message));
				return false;
			}
		}
	}

	public class SendPPResponse : FinalizeTrxn
	{
		internal async override Task<bool> finalizeTrxn(StateObject state, TransactionManagement transactionManagement)
		{
			try
			{
				return false;
			}
			catch (Exception e)
			{
				TransMng.Error(string.Format("Error: {0}", e.Message));
				return false;
			}
		}
	}
}
